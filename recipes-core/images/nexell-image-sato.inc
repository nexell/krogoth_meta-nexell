# Base this image on core-image-sato
include recipes-sato/images/core-image-sato.bb
LICENSE = "GPLv2"
# Include modules in rootfs
IMAGE_INSTALL += " \
        kernel-modules \
	"
